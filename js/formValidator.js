/* 
 * 表单验证
 * formValidator.js
 * Copyright (c) 2012 vakinge@gmail.com
 */
/*form.validator.js*/
;(function($) {
	//  表单验证
	$.fn.formValidator = function(options) {
		var settings = $.ValidatorRules; 
		this.each(function(){
			var $form = $(this);
			if(!$form.attr('id'))$form.attr('id','form_'+new Date().getTime());
			$('*[dataType]',$form).on('focus',function(index){
				var _this = $(this),
				define = eval('settings.'+_this.attr('dataType')),
				focusTip = _this.attr('focusTip') || define.tip;
			    getTipElement(_this,index).attr('class','onFocus').html(focusTip || '&nbsp;');
			}).on('blur',function(index){
			  doValidateSingleInput(this,index);
		    });
			
			$form.submit(function(){
				return doValidateForm($(this));
		    });
		});
    };
	

	function doValidateSingleInput(formElement,index) {
		var settings = $.ValidatorRules; 
		var _this = $(formElement), _dataType = _this.attr('dataType'),
		    define = eval('settings.'+_dataType),
		    val = _this.val(), result = false;
		if(formElement.disabled)return true;
		if(_this.attr("require") == "false" && val == "")return true;
		switch (_dataType) {
		case "Date":
			result = eval('settings.' + _dataType + '(_this)');
			break;
		default:
			result = define.expr.test(val);
			break;
		}
		if(result){
			getTipElement(_this,index).attr('class','onCorrect').html(settings.CorrectText);
        }else{
        	var errorTip = _this.attr('errorTip') || define.errorTip;
          	getTipElement(_this,index).attr('class','onError').html(errorTip);
        }
		return result;
	}
	
	function doValidateForm($form){
		var settings = $.ValidatorRules,$fileds = $('*[dataType]',$form);
		if($fileds.length == 0)return true;
		var result = false;
		$fileds.each(function(index){
			result = doValidateSingleInput(this,index);
            return result;
		});
		return result;
	}
	
	function getTipElement($obj,index){
		var id = $obj.attr('id');
		if(!id){
			id = $obj.attr('name') ||  "obj"+index;
			id = id.replace(/[^0-9a-zA-Z]/g, "");
			$obj.attr('id',id);
		}
		id = id + 'Tip';
		!$('#'+id)[0] && $obj.after('<div id="'+id+'" class="onError">&nbsp;</div>');
		return $('#'+id);
	}
	
	$.fn.doFormValidator = function(){
		var $form = $(this);
		return doValidateForm($form);
	},
	
	//默认验证规则
	$.ValidatorRules = {
			Require : {expr:/.+/,tip:"必填",errorTip:"该字段不能为空"},
		    Email : {expr:/^\w+([-+.]\w+)*@\w+([-.]\\w+)*\.\w+([-.]\w+)*$/,tip:"电子邮箱",errorTip:"Email格式不正确"},
		    Mobile:{expr:/^(1[3|5|8]{1}\d{9})$/,tip:"手机号码",errorTip:"手机格式不正确"},
		    TelePhone:{expr:/^(((0\d{2,3}-)?\d{7,8}(-\d{4})?))$/,tip:"电话号码",errorTip:"电话号码格式不正确"},
		    IdCard:{expr:/^\d{15}(\d{2}[A-Za-z0-9])?$/,tip:"身份证号码",errorTip:"身份证号码格式不正确"},
		    Integer:{expr:/^\d+$/,tip:"正数",errorTip:"仅支持整数"},
		    Number:{expr:/^-?(\d+|[1-9]\d*\.\d+|0\.\d*[1-9]\d*|0?\.0+|0)$/,tip:"数字字符",errorTip:"仅支持数字"},
		    English:{expr:/^[A-Za-z]+$/,tip:"英文字符",errorTip:"仅支持英文字符"},
		    ZipCode:{expr:/^[1-9]\d{5}(?!\d)$/,tip:"邮政编码",errorTip:"邮政编码格式不正确"},
		    Chinese:{expr:/^[\u0391-\uFFE5]+$/,tip:"中文字符",errorTip:"仅支持中文字符"},
		    URL:{expr:/^http:\/\/[A-Za-z0-9]+\.[A-Za-z0-9]+[\/=\?%\-&_~`@[\]\':+!]*([^<>\"\"])*$/,tip:"网址URL",errorTip:"URL地址格式不正确"},
		    Regex:{tip:"",errorTip:"格式不正确"},
		    UnSafe : /^(([A-Z]*|[a-z]*|\d*|[-_\~!@#\$%\^&\*\.\(\)\[\]\{\}<>\?\\\/\'\"]*)|.{0,5})$|\s/,	
	        ErrorText:'&nbsp;',
	        CorrectText:'&nbsp;',
	        CompareEquals:function($obj){
	        	var _compareWith = $obj.attr('compareWith');
	        	return $obj.val() != '' && $obj.val() == $(_compareWith).val();
            }
	}; 
})(jQuery);