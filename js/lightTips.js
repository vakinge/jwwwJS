/* 
 * lightTips.js
 * Copyright (c) 2013 vakinge@gmail.com
 */
;(function($) {
    $.extend($,{
		successTip : function(msg){
		  $.lightTips({content:msg, icon:'success'});
	    },
	    alertTip : function(msg){
	    	$.lightTips({content:msg, icon:'alert'});
	    },
		errorTip : function(msg){
	    	$.lightTips({content:msg, icon:'error'});
	    }
	});
	// 提示信息
	$.lightTips = function(options) {
		var settings = {
			content : '',
			icon : 'success',
			time : 1500,
			close : false,
			zindex : 2999
		};
		if (options) {
			$.extend(settings, options);
		}
		if (settings.close) {
			$(".tipbox").hide();return;
		}
		if (!$('.tipbox')[0]) {
			$('body').append('<div class="tipbox"><div class="tip-l"></div><div class="tip-c"></div><div class="tip-r"></div></div>');
			$('.tipbox').css('z-index', parseInt(settings.zindex));
		}
		$('.tipbox').attr('class', 'tipbox tip-' + settings.icon);
		$('.tipbox .tip-c').html(settings.content);
		$('.tipbox').css('z-index', parseInt($('.tipbox').css('z-index')) + 1).setmiddle().show();

		if (settings.time > 0) {
			setTimeout(function() {
				$(".tipbox").fadeOut();
			}, settings.time);
		}
	};
})(jQuery);

// 把对象调整到中心位置
;(function($) {
	
	$.fn.setmiddle = function() {
		var dl = $(document).scrollLeft(), dt = $(document).scrollTop(), ww = $(
				window).width(), wh = $(window).height(), ow = $(this).width(), oh = $(
				this).height(), left = (ww - ow) / 2 + dl, top = (oh < 4 * wh / 7 ? wh
				* 0.382 - oh / 2 : (wh - oh) / 2) + dt;
		$(this).css({ left : Math.max(left, dl) + 'px', top : Math.max(top, dt) + 'px'});
		return this;
	};
})(jQuery);